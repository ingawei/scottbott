
// require discord.js module
const Discord = require('discord.js');
const auth = require('./auth.json');

/**
 * configuration
 *  -1: random
 *  > -1: corresponding converter in the converter map
 */
const config = require('./config.json');

// converter imports
const duhHDUh = require('./mocks/duhHDUh.js');
const sCoTT = require('./mocks/sCoTT.js');

// map of possible message converters
const converters = {
  0: duhHDUh,
  1: sCoTT
}

// create a new discord client
const client = new Discord.Client();

function isLetter(str) {
  return str.length === 1 && str.match(/[a-z]/i);
}

client.on('message', message => {

  var targetedUser = true;
  const emoji = message.guild.emojis.cache.find(emoji => emoji.name === 'just_scott_doing_what_he_does_ev');

  if (message.content.startsWith('Inga is better than me at everything')) {
    targetedUser = false;
  } else if (message.author.username === 'Scótt') {
    message.react(emoji);
  } else {
    targetedUser = false;
  }

  if (targetedUser) {
    let mock;

    if (/[^\u0000-\u00ff]/.test(message.content)) {
      mock = `${message.author.username} said You know how I feel, why would you say that? Like, you put me in such an uncomfortable situation. Like, you know I'm not happy and you know I'm trying to see if it'll work out here, and I know that it's not`
    } else {
      var converter;

      if (config.mockType > -1) {
        converter = converters[config.mockType];
      } else {
        const rnd = Math.floor(Math.random() * 2);
        converter = converters[rnd];
      }

      mock = converter(message);
    }

    message.channel.send(mock);
  }
});

// login to Discord with this token
client.login(auth.token);