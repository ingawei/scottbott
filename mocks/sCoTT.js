/**
 * Mocks Scott's messages in an immature fashion.
 * 
 * @param {Object} message - The original stupid message.
 */
function sCoTT(message) {
  const content = message.content.toLowerCase();
  const mock = [];
  let lower = true;
  let counter = 0;

  for (let i = 0; i < content.length; i++) {
    const char = content.charAt(i);

    if (char === ' ') {
      mock.push(' ');
    } else {
      mock.push(lower ? char : char.toUpperCase());

      const rand = Math.floor(Math.random() * 2) - 1;
      if (rand < counter) {
        lower = lower ? false : true;
        counter = 0;
      } else {
        counter++;
      }
    }
  }

  return mock.join('');
}

module.exports = sCoTT;