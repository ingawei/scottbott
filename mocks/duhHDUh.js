function isLetter(str) {
  return str.length === 1 && str.match(/[a-z]/i);
}


/**
 * Makes Scott's messages sound more like the actual Scott.
 * 
 * @param {Object} message - The original stupid message.
 */
function duhHDUh(message) {
  var content = message.content;
  var template = 'duHDUh';
  var mocking = '';
  var tracker = 0;

  for (var i = 0; i < content.length; i++) {
    var letter = content.charAt(i);
    if (isLetter(letter)) {
      mocking = mocking + template.charAt(tracker);
      tracker = (tracker + 1) % template.length;
    } else {
      mocking = mocking + letter;
    }
  }

  return mocking;
}

module.exports = duhHDUh;
